# while & for loops

# while loop - executes a block of code while a condition is true
# value = 1
"""while value <= 10:
    print(value)
    if value == 5:
        break
    value +=1"""


"""while value <= 10:
    value += 1
    if value == 5:
        continue
    print(value)
else:
    print("Value is now equal to " + str(value))
   """ 

# for loop - iterates over a sequence 

names = ["Dave", "John", "Clark"]
"""for y in names:
    print(y)

#you can go through the characters
for x in "DevOps_Engineer":
    print(x)
"""

"""for z in names:
    if z == "John":
        break
    print(z)
    """
"""for h in names:
    if h == "Dave":
        continue
    print(h)

for x in range(2,4):
    print(x)
"""

for x in range(0, 100, 5):  # increment by 5 
    print(x)
else:
    print("Glad that's over")



actions = ["codes", "eats", "sleeps"]

for name in names:
    for action in actions:
        print(name + " " + action + ".")
        